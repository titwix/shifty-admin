export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Shifty - Admin',
    htmlAttrs: {
      lang: 'fr'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Shifty, site de reservation de restaurant' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        src: "https://kit.fontawesome.com/1146ba0a15.js",
        body: true
      },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/style.scss'
  ],

  // Color loading line
  loading: {color: '#3d40ff'},

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/axios',
    "~/plugins/notification/index.js"
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/dotenv',
  ],

  //Moment params
  moment: {
    defaultLocale: 'fr',
    locale: 'fr'
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/moment',
    '@nuxtjs/universal-storage'
  ],

  storage: {
    vuex: {
      namespace: 'storage'
    }
  },

  axios: {
    baseURL: process.env.API_BASE_URL+'/'+process.env.VERSION+'/admin'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  auth: {
    strategies: {
      local: {
        token: {
          property: 'body.token',
        },
        user: {
          property: 'body',
        },
        endpoints: {
          login: { url: '/sessions', method: 'post' },
          logout: { url: '/sessions', method: 'delete' },
          user: { url: '/users/me', method: 'get' }
        }
      }
    },
    redirect: {
      login: '/portal',
      logout: '/login',
      callback: '/',
      home: '/dashboard'
    }
  }
}
