export default async function ({$axios, $auth, redirect, route}) {
    
    if ($auth.loggedIn) {
        if(!$auth.$storage.getUniversal("restaurantId")) {
            return redirect('/portal')
        } else {
            const restaurantId = $auth.$storage.getUniversal("restaurantId")

            return await $axios.$get(`/restaurants/${restaurantId}`).then(async data => {
        
                await $auth.$storage.setUniversal("restaurant", data.body)
        
                return true
            }).catch(err => {
                return redirect('/portal')
            })
        }
    } else {
        return redirect('/login')
    }
}