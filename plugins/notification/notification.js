import notification from '~/plugins/notification/notification.vue'

const Plugin = {
  install(Vue, options = {}) {
    /**
     * Makes sure that plugin can be installed only once
     */
    if (this.installed) {
      return
    }
    this.installed = true

    /**
     * Create event bus
     */

    this.event = new Vue()

    /**
     * Plugin methods
     */
    Vue.prototype.$notification = {
      show(options = {}) {
        // show notification
        Plugin.event.$emit('show', options)
      }
    }

    /**
     * Registration of <notification/> component
     */
    Vue.component('notification', notification)
  }
}

export default Plugin